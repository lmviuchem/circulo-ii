import pygame
import sys
import random

pygame.init()

screen = pygame.display.set_mode((500,500))
pygame.display.set_caption("Rebote de un Círculo")

#Variables para la creación del círculo
color = (0,0,0)
posX = 250
posY = 250
radio = 20

#Direccion
dirX = random.choice([-3, 4])
dirY = random.choice([-5, 2])   
             
time = pygame.time.Clock()

while True:
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit() 
    
    #Movimiento del Círculo
    posX = posX + dirX
    posY = posY + dirY
    
    #Rebote
    if posX - radio <= 0:
        dirX = -dirX
    if posX + radio >= 500:
        dirX = -dirX
    if posY - radio <= 0:
        dirY = -dirY
    if posY + radio >= 500:
        dirY = - dirY
    
    screen.fill((255,255,255))
    circulo = pygame.draw.circle(screen, color, (posX, posY), radio)
    
    time.tick(120)
    
    pygame.display.update()    